@extends('layout.app')
@section('content')


<div class="show-container">

@if($errors->any())
    {!! implode('', $errors->all('<div class="message">:message</div>')) !!}
@endif
<div class="show-title">عدد دلخواه را وارد کنید</div>

<div class="wrapper-form"> 
<form  action ="{{ route('triangle.pascal') }}" method="POST">
    @csrf
  <div class="form-group ">
    <input type="text"  class="input" name="number" placeholder="please enter a number">
  </div>
  
  <button type="submit" class="btn btn-pink ">Confirm identity</button>
</form>
</div>
</div>
@if(isset($triangle))
@foreach($triangle as $alls)
    @foreach($alls as $all)
        <div class = "wrapper-row">
            @foreach($all as $item)
                <div class="rows">
                    <?= $item; ?>
                </div>
            @endforeach
        </div>
        <br>
    @endforeach

@endforeach
@endif
@endsection