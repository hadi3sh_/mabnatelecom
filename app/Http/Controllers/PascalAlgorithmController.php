<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PascalRequest;

class PascalAlgorithmController extends Controller
{
    public function index()
    {
        return view('index');
    }

    /**
     * redirect to pascal page 
     */
    public function pascal()
    {
        return view('pascal');
    }

        /**
     * redirect to pascal factorial 
     */
    public function factorial()
    {
        return view('factorial');
    }


    public function pascalMethod(PascalRequest $request)
    {
        $numb = $request->number;
        if($numb == 0){
            $triangle = [];
        }
        for ($line = 1; $line <= $numb ; $line++) {
            $calculating = 1;
            for ($i = 1,$index = 0; $i <= $line; $i++ ,$index++) {
                $eachRow = [];
                $result[$index] = $calculating;
                $eachRow[] = $result;
                $calculating = $calculating * ($line - $i) / $i;
            }  
            $triangle[] = $eachRow;
        }
        return view('pascal',['triangle'=>$triangle]);
    }

    public function factorialMethod(PascalRequest $request)
    {
        $numb = $request->number;
        if($numb == 0){
            $triangle = [];
        }
        for($i = $numb ; $i>=1 ;$i--){
           $triangle[]= $this->subFuctorial($numb-$i);
        }
      
        return view('factorial',['triangle'=>$triangle]);
    }
    

    private function fact(int $num)
    {
        $fact = 1;
        for($i = 1 ; $i<=$num ; $i++)
        $fact *= $i; 
        return $fact;
    }

    private function subFuctorial(int $n)
    {
        for($i = 0 ; $i<=$n ; $i++){
            $eachRow =[]; 
           
                $result[$i] = $this->fact($n) / ($this->fact($i) * $this->fact($n-$i));
                $eachRow[] = $result;
                
            }
        return  $eachRow;
    }

}
