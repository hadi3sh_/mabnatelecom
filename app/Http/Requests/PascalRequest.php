<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PascalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required|numeric',
        ];
    }

    public function messages()
    {
        return [
            'number.required' => 'A Number is required',
            'number.numeric' => 'A Number must be integr',
        ];
    }
}
