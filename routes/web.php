<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PascalAlgorithmController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', [PascalAlgorithmController::class, 'index']);
Route::get('/pascal', [PascalAlgorithmController::class, 'pascal'])
->name("pascal");
Route::get('/factorial', [PascalAlgorithmController::class, 'factorial'])
->name("factorial");

Route::post('/pascal',
[PascalAlgorithmController::class, 'pascalMethod'])
->name("triangle.pascal");

Route::post('/factorial',
[PascalAlgorithmController::class, 'factorialMethod'])
->name("triangle.factorial");
